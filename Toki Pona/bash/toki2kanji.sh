#! /bin/bash

ORIG="$1"
TRAD="${ORIG%%.*}"
cp -v $1 $TRAD.kanji.md

declare -a tokipona=( kepeken sitelen kulupu kalama kipisi pimeja sijelo pakala palisa namako sinpin soweli alasa akesi linja lukin monsi kiwen nanpa tenpo nasin pilin utala anpa ante awen esun insa jaki jelo kala kasi kama kili pini pipi poka poki pona sama seli selo seme tomo unpa walo kule kute lape lete lili lipu laso lawa waso mama mani meli luka lupa loje pana pata sina sona suli suno supa suwi toki taso tawa telo moku moli mije wawa weka musi mute wile nasa nena nimi noka olin pali open sike sewi lon kon len mun ala ale anu ali pan tan uta wan ijo ike ilo jan ken oko sin ona kin en jo la ko li ma mi mu ni tu pi a e o )

declare -a kanji=( 使 画 群 音 切 黒 体 打 棒 冗 前 猫 探 獣 糸 見 後 石 番 时 道 心 戦 下 变 待 市 内 汚 黄 魚 木 来 果 終 虫 側 箱 良 同 火 皮 何 家 盛 白 色 聞 眠 冷 小 葉 青 首 鳥 母 貝 女 手 穴 赤 授 氏 君 知 大 日 面 甜 言 許 去 水 食 死 男 力 遥 楽 多 要	狂 丘 称 足 愛 作 開 丸 上 在 空 布 月 無 全 ぬ 全 米 因 口 一 物 悪 具 人 能 目 新 彼 又 ん 有 ら 粉 り 土 私 む 此 二 ぴ あ え お )

for i in `seq 1 ${#tokipona[@]}` ; do
	sed -i "s/${tokipona[i-1]}/${kanji[i-1]}/g" $TRAD.kanji.md
done

sed -i 's/ //g' $TRAD.kanji.md
sed -i 's/¡//g' $TRAD.kanji.md
sed -i 's/¿//g' $TRAD.kanji.md

sed -i 's/\./。/g' $TRAD.kanji.md

echo DONE!
